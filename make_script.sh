#!/bin/bash
#add terra download and install (includes, and lib)

case "$OSTYPE" in 
	linux*)
		g++ -O2 -shared -o libluacsound.so -fPIC terra_init.cpp -I/usr/local/include/csound/ -I/usr/include/luajit-2.1 -L/usr/lib/x86_64-linux-gnu -l "luajit-5.1" -std=c++11	
		;;
	
	msys*)
		g++ -O2 -shared -o libluacsound.dll -fPIC terra_init.cpp -DUSE_DOUBLE \
			-I "C:/Program Files/Csound6_x64/csound/include" -I "C:/Program Files/Csound6_x64/csound/include/csound" \
		       	-I "C:/Program Files/LuaJIT/include" \
		       -L "C:/Program Files/Csound6_x64/csound" -llua51 -std=gnu++11	
		;;
	darwin*)
		clang++ -dynamiclib -o libluacsound.dylib terra_init.cpp \
		-I /Volumes/MacOSX/Library/Frameworks/CsoundLib64.framework/Versions/Current/Headers/ \
		-I /usr/local/include/luajit-2.1 \
		-L /usr/local/lib -std=c++11 -lluajit-5.1\
		-std=c++11
		#cp libluacsound.dylib /Volumes/MacOSX/Library/Frameworks/CsoundLib64.framework/Resources/Opcodes64

		;;
	*)
esac
